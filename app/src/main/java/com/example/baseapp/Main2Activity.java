package com.example.baseapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    TextView tvCount; // oncreate에서 화면이 구성되어야만 생성!
    Button btnCount, btnWeb, btnCall;
    private int cnt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        tvCount = findViewById(R.id.tvCount);
        btnCount = findViewById(R.id.btnCount1);
        btnWeb = findViewById(R.id.btnWeb);
        btnCall = findViewById(R.id.btnCall);

        btnWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com"));

                startActivity(intent);

            }
        });
        btnCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tvCount.setText(++cnt + "");

            }
        });


        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:010-1111-1111"));

                startActivity(intent);

            }
        });


    }
}
