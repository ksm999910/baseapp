package com.example.baseapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

public class RechatActivity extends AppCompatActivity {

    TextView tvMsg;
    private Button btnSend;
    private EditText etMsg;
    private ScrollView sv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rechat);

        tvMsg = findViewById(R.id.tvMsg);
        btnSend = findViewById(R.id.btnSend);
        etMsg = findViewById(R.id.etMsg);
        sv = findViewById(R.id.scrollView);

        etMsg.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                switch (keyCode) {
                    case KeyEvent.KEYCODE_ENTER:
                        String msg = etMsg.getText().toString().trim(); // 방어코드! 예외처리 등 실무에서 사용많이함!
                        if(msg.equals("") == false ) {//문자열 그대로만나면 메모리위치가 달라지므로!! equals를 사용한다!
                            tvMsg.append("\n" + msg);
                            etMsg.setText("");
                            sv.fullScroll(ScrollView.FOCUS_DOWN);
                            etMsg.requestFocus(); // 자기자신에 포커스를 유지
                            // 키코드가 엔터면 Send 실행!


                        }

                }
                return false; // true를 사용할시 이벤트가 즉각 끝나버림. 지속적인 작업을 위해서 false
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = etMsg.getText().toString();
                //  msg  += etMsg.getText().toString();
                // tvMsg.setText(msg);
                tvMsg.append("\n" + msg);
                etMsg.setText("");
                sv.fullScroll(ScrollView.FOCUS_DOWN);


            }
        });

    }
}
