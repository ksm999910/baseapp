package com.example.baseapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CountActivity extends AppCompatActivity {
    int cnt;
    Button btnCount1;
    TextView tvCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count);




        tvCount = findViewById(R.id.tvCount);
        btnCount1 = findViewById(R.id.btnCount1);


        btnCount1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                tvCount.setText(++cnt + "");


            }
        });


    }
}
