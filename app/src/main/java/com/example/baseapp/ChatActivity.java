package com.example.baseapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ChatActivity extends AppCompatActivity {

    TextView tvMsg;
    Button btnSend;
    EditText etMsg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        tvMsg = findViewById(R.id.tvMsg);
        btnSend = findViewById(R.id.btnSend);
        etMsg = findViewById(R.id.etMsg);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg  = etMsg.getText().toString();
               //  msg  += etMsg.getText().toString();
                // tvMsg.setText(msg);
                tvMsg.append("\n"+msg);
                etMsg.setText("");
            }
        });



    }
}
